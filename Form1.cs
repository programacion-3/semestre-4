﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label5.Text = "0";
            textBox1.Text = "0";
            textBox2.Text = "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Double a = Convert.ToDouble(textBox1.Text);
            Double b = Convert.ToDouble(textBox2.Text);
            Double c = a + b;
            label5.Text = c.ToString() ;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Double a = Convert.ToDouble(textBox1.Text);
            Double b = Convert.ToDouble(textBox2.Text);
            Double c = a - b;
            label5.Text = c.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Double a = Convert.ToDouble(textBox1.Text);
            Double b = Convert.ToDouble(textBox2.Text);
            Double c = a * b;
            label5.Text = c.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Double a = Convert.ToDouble(textBox1.Text);
            Double b = Convert.ToDouble(textBox2.Text);
            Double c = a / b;
            label5.Text = c.ToString();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            label5.Text = "0";
            textBox1.Text = "0";
            textBox2.Text = "0";
        }
    }
}
